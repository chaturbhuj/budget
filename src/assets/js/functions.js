(function($) {
	"use strict";

	/*  [ Responsive menu ]
	- - - - - - - - - - - - - - - - - - - - */
	$( '.mobile-menu' ).dlmenu();

	/*  [ Sticky menu ]
	- - - - - - - - - - - - - - - - - - - - */
	$( '#site-header .bot' ).scrollFix({
		fixClass: 'sticky',
	});

	/*  [ Custom RTL Menu ]
	- - - - - - - - - - - - - - - - - - - - */
	$( '.sub-menu li' ).hover( function() {
		var sub_menu = $( this ).find( ' > .sub-menu' );
		if ( sub_menu.length ) {
			if ( sub_menu.outerWidth() > ( $( window ).outerWidth() - sub_menu.offset().left ) ) {
				$( this ).addClass( 'rtl' );
			}
		}
	});

})(jQuery);