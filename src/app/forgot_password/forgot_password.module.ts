import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ForgotPasswordComponent } from './forgot_password.component';
import { routing } from './forgot_password.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
  ForgotPasswordComponent
  ],
  bootstrap: [
   ForgotPasswordComponent
  ]
})
export class ForgotPasswordModule {}
