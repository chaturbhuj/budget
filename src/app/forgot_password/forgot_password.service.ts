import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ForgotPasswordService {

  private posts;
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

  constructor(public http: Http) {
	
	
  }

  forgot_password_user(API_URL,user_email){
	
	var params = {
		user_email:user_email,
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/send_email_forget_password', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
  }


}
