import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { ForgotPasswordService } from './forgot_password.service';
import { ConstantService } from '../constant/constant.service';


import 'rxjs/add/operator/map';

@Component({
  selector: 'app-forgot_password',
  templateUrl: './forgot_password.component.html'
})
export class ForgotPasswordComponent implements OnInit {
	/*Variable define */
	user_email_n = '';
	private responseData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _forgotPasswordService: ForgotPasswordService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
	}
	
	
	ngOnInit() {}
	
	/* This part of script is used for user registration*/
	userForgotPassword() {
		var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
	
		if (!this.user_email_n) {
			this.toasterService.pop('error', 'Error', 'Email is required.');			
		}else if (!EMAIL_REGEXP.test(this.user_email_n)) {
			this.toasterService.pop('error', 'Error', 'Email is not valid.');
		}else{
			this._forgotPasswordService.forgot_password_user(this._constantService.API_URL,this.user_email_n)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						if(((this.responseData)).status == 200) {                                                                                               
							this.toasterService.pop('success','Success', ((this.responseData)).message);
						}else  {
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}
					}
				);
			
			console.log(this.user_email_n);
			
			/*this.router.navigate(['/login']);*/
		}
		
	}
	
}
