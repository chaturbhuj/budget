import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ForgotPasswordComponent } from './forgot_password.component';

const routes: Route[] = [
  {
    path: '',
    component: ForgotPasswordComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
