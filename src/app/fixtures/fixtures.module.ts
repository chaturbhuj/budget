import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FixturesComponent } from './fixtures.component';
import { routing } from './fixtures.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    FixturesComponent
  ],
  bootstrap: [
    FixturesComponent
  ]
})
export class FixturesModule {}
 