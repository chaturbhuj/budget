import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { FixturesComponent } from './fixtures.component';

const routes: Route[] = [
  {
    path: '',
    component: FixturesComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
 