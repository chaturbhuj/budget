import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class FixturesService {

	constructor(public http: Http) {
	
	}

	get_details_fixtures(API_URL,sportId,leagueId){
		
		var params = {
			myurl:'v1/fixtures?sportid='+sportId+'&leagueid='+leagueId,  
		
		};
		
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'configure/get_list_of_sports', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
  

}
