import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from '@angular/router';

import {FixturesService } from './fixtures.service';
import { ConstantService } from '../constant/constant.service';


declare var $: any;

@Component({ 
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html'
})
export class FixturesComponent {
  /*Variable define */
	
	sportId = 1;
	leagueId = 2;
	
	public fixturesData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private route: ActivatedRoute, private _fixturesService: FixturesService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		
 
		this._fixturesService.get_details_fixtures(this._constantService.API_URL,this.sportId,this.leagueId)
			.subscribe(
				data => this.fixturesData = (data.leagues),
				error => alert(error),
				() => {
				console.log(this.fixturesData);
				
				}
			);
			 
	}
		
	ngOnInit() {} 
		
}
