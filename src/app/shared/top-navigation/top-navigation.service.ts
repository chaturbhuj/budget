import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class TopNavigationService {

  private posts;
  private url = 'http://jsonplaceholder.typicode.com/posts'
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

	constructor(public http: Http) {
	
	
	}

	userloginsubmit(API_URL, user_email, passwd){
	
		var params = {
			user_email:user_email,
			passwd:passwd
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'users/login', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
	register_user(API_URL, mrandms, user_email, dateofbirth, rapidAddress, user_name, firstname, lastname, passwd){
		
		var params = {
			mrandms:mrandms,
			user_email:user_email,
			dateofbirth:dateofbirth,
			rapidAddress:rapidAddress,
			user_name:user_name,
			firstname:firstname,
			lastname:lastname,
			passwd:passwd
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'users/register', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}

}
