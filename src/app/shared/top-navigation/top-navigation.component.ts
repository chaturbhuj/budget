import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
 
import { TopNavigationService } from './top-navigation.service';
import { ConstantService } from '../../constant/constant.service';

import 'rxjs/add/operator/map'; 

declare var $: any;



@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.css']
}) 


export class TopNavigationComponent implements OnInit {
	/*Variable define */
	loggedinUserName = '';
	userEmail = '';
	userPassword = '';
	
	user_email = '';
	firstname = '';
	lastname = '';
	dateofbirth = '';
	rapidAddress = '';
	user_name = '';
	passwd = '';
	cfm_passwd = '';
	termsandconditions = '';
	private responseData:any = [];
	
	publicContent = true;
	privateContent = false;
	form: FormGroup;
	
	private toasterService: ToasterService;
	private responseData_signup:any = [];
	
	/*loginForm: FormGroup; Form initialize*/
	
	@ViewChild('topnav') topnav: ElementRef;
	
	
	private newpost:any = [];
	private myvalue = {'myvalue':'myvalue'};
	
	
	
	cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;

  message: string;

  
  
  
	constructor(private router: Router, private _constantService: ConstantService, private topNavigationService: TopNavigationService, private _http:HttpModule, public fb: FormBuilder, toasterService: ToasterService) {
		this.toasterService = toasterService;
		var currentUser = JSON.parse(localStorage.getItem('budgetUser'));
		console.log(currentUser);
		if(currentUser){
			this.loggedinUserName = (currentUser.user_name);
			this.publicContent = false;
			this.privateContent = true;
		}else{
			console.log('currentUser'); 
		}	
		/*
		var handler = (<any>window).StripeCheckout.configure({
			  key: 'pk_test_4OBAviq132YMfBpwTtJUBGAX',
			  locale: 'auto',
			  token: 'tok_1AUVMgD1EcqGcBwKLyvfxbdN'
			});

			handler.open({
			  name: 'Demo Site',
			  description: '2 widgets',
			  amount: 2000
			});
			
		(<any>window).Stripe.card.createToken({
		  number: 4242424242424242,
		  exp_month: 2,
		  exp_year: 18,
		  cvc: 222
		}, (status: number, response: any) => {
		console.log(response.id);
		console.log(response);
		  if (status === 200) {
		  
			this.message = `Success! Card token ${response.card.id}.`;
		  } else {
			this.message = response.error.message;
		  }
		});*/
		
	
	}
	
	 
	ngOnInit() {}
	
	/*This part of script is used for user login*/
	userLogin() {
		
		if (!this.userEmail) {
			this.toasterService.pop('error', 'Error', 'Email is required.');			
		}else if (!this.userPassword) {
			this.toasterService.pop('error', 'Error', 'Password is required.');		
		}else{ 
			console.log(this.userEmail);
			console.log(this.userPassword);
			this.topNavigationService.userloginsubmit(this._constantService.API_URL, this.userEmail, this.userPassword)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						if(((this.responseData)).status == 200) {
							this.toasterService.pop('success', 'Success', ((this.responseData)).message);
							localStorage.setItem('budgetUser', JSON.stringify((this.responseData).data));
							var currentUser = JSON.parse(localStorage.getItem('budgetUser'));
							console.log(currentUser);
							if(currentUser){
								this.loggedinUserName = (currentUser.user_name);
								this.publicContent = false;
								this.privateContent = true;
							}	
							this.router.navigate(['/profile_setting']);
						}else{
							this.toasterService.pop('error', 'Error', ((this.responseData)).message);
						}
					}
				);
		}
	}

	/* This part of script is used for open registration pop*/
	popuptoregister() {
		$('body').on('click','.popuptoregister',function(){
			$('#itemsModal').modal('show');
		});
	}	
	
	/* This part of script is used for logout user*/
	logoutUser() { 
		localStorage.setItem('budgetUser', JSON.stringify(null));
		var currentUser = JSON.parse(localStorage.getItem('budgetUser'));
		console.log(currentUser);
			
	}	
	
	/* This part of script is used for user registration*/
	userRegistration() {	
		console.log($("#termsandconditions").attr("checked"));
		console.log($('#dateofbirth').val());
		var mrandms = ($('input[name="mrandms"]:checked').val());
		var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
		if (!this.user_email) {
			this.toasterService.pop('error', 'Error', 'Email is required.');
		}else if (!EMAIL_REGEXP.test(this.user_email)) {
			this.toasterService.pop('error', 'Error', 'Email is not valid.');
		}else if (!$('#dateofbirth').val()) {
			this.toasterService.pop('error', 'Error', 'Bate of birth is required.');		
		}else if (!this.rapidAddress) {
			this.toasterService.pop('error', 'Error', 'Addres is required.');		
		}else if (!this.user_name) {
			this.toasterService.pop('error', 'Error', 'User name is required.');		
		}else if (!this.firstname) {
			this.toasterService.pop('error', 'Error', 'First name is required.');		
		}else if (!this.lastname) {
			this.toasterService.pop('error', 'Error', 'Last name is required.');		
		}else if (!this.passwd) {
			this.toasterService.pop('error', 'Error', 'Password is required.');		
		}else if (!this.cfm_passwd || this.cfm_passwd != this.passwd) {
			this.toasterService.pop('error', 'Error', 'Confirm Password is required and should be same as password.');		
		}else if ($("#termsandconditions").attr("checked") != 'checked') {
			this.toasterService.pop('error', 'Error', 'Please confirm terms and conditions.');		
		}else{
			this.topNavigationService.register_user(this._constantService.API_URL, mrandms, this.user_email, $('#dateofbirth').val(), this.rapidAddress, this.user_name,  this.firstname, this.lastname, this.passwd)
				.subscribe(
					data => this.responseData_signup = (data),
					error => alert(error),
					() => {
						console.log(this.responseData_signup);
						if(((this.responseData_signup)).status == 200) {
							this.toasterService.pop('success', 'Success', ((this.responseData_signup)).message);
							$('#itemsModal').modal('hide');
						}else{
							this.toasterService.pop('error', 'Error', ((this.responseData_signup)).message);
						}
					}
				);
			
			/*this.router.navigate(['/login']);*/
		}		
	}
	
	toggle() {
		this.topnav.nativeElement.classList.toggle(['responsive']);
	}

}
