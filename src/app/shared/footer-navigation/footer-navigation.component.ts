import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-ui-footer-navigation',
  templateUrl: './footer-navigation.component.html'
})
export class FooterNavigationComponent implements OnInit {

  @ViewChild('footernav') topnav: ElementRef;

  constructor() { }

  ngOnInit() {}

  toggle() {
    this.topnav.nativeElement.classList.toggle(['responsive']);
  }

}
