import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { WithdrawService } from './withdraw.service';
import { ConstantService } from '../constant/constant.service';


import 'rxjs/add/operator/map';

	declare var $: any;
	
@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html'
})
export class WithdrawComponent implements OnInit {

	/*Variable declaration */

	logged_user_id = 0;
	bank_id = 0;
	branch_code = '';  
	bank_name = '';
	account_no = '';
	userbanklist = '';
	user_id = 0;
	transaction_id = 0;
	withdrawamount= '';
	withdraw_id= 0;
	withdraw_type= '';
	withdraw_amount= ''; 
	usersecurityanswer =''; 
	security_question =''; 
	no_of_attempts =0; 
	no_of_attempts_left =0; 
	
	public responseData:any = [];
	public responseBankData:any = []; 
	public responseBankData_:any = []; 
	public responseDataRemove:any = []; 
	public responsesecurityquestionData:any = []; 
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _withdrawService: WithdrawService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		/* This script is used to read user session data from his local storage */
		var currentUser = JSON.parse(localStorage.getItem('budgetUser')); 
		console.log(currentUser);
		if(currentUser){
			this.logged_user_id = currentUser.userID;
		} else{
			this.router.navigate(['/home']);
		}
		
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open'); 
		$('body').css({'padding':'0px'});
		this._withdrawService.get_user_bank_details(this._constantService.API_URL,this.logged_user_id)
			.subscribe(
				data => this.responseBankData_ = (data),
				error => alert(error),
				() => {
					this.responseBankData = this.responseBankData_.data;
					this.security_question = this.responseBankData_.security_question;
					this.no_of_attempts = this.responseBankData_.no_of_attempts;
					this.no_of_attempts_left = 3 - this.responseBankData_.no_of_attempts;
					
				}
			);
	}
	
	
	ngOnInit() {}
	
	
	/* This part of script is used for open faq pop*/
	securityQuestionPopupOpen() {
		$('body').on('click','.securityQuestionPopupOpen',function(){
			$('#security_question_popup').modal('show');
		});
	}	
	
	
	/* This part of script is used for open add bank pop*/
	bankPopupOpen() {
		this.withdraw_id = 0; 
		this.bank_name = ''; 
		this.branch_code = ''; 
		this.account_no = ''; 
		$('body').on('click','.bankPopupOpen',function(){
			$('#banksModal').modal('show');
		});
	}	
	
	/*This part of script is used for edit bank*/
	bankEditPopupOpen(my_bank_id, my_bank_name, my_branch_code, my_account_no) {
		this.bank_id = my_bank_id; 
		this.bank_name = my_bank_name; 
		this.branch_code = my_branch_code; 
		this.account_no = my_account_no; 
		$('body').on('click','.bankEditPopupOpen',function(){
			$('#banksModal').modal('show');
		});
	}
	
	/*This part of script is used for remove bank*/
	
	bankRemove(my_bank_id){
		this._withdrawService.common_remove_bank(this._constantService.API_URL, this.logged_user_id, my_bank_id, 'budgetget_user_bank', 'bank_id')
		.subscribe(
			data => this.responseDataRemove = (data),
			error => alert(error),
			() => {
				console.log(this.responseDataRemove);
				if(((this.responseDataRemove)).status == 200) {                                                                                               
					this.toasterService.pop('success','Success', ((this.responseDataRemove)).message);
					this.router.navigate(['redirect', {redirect: 'withdraw'}]);
				}else  {
					this.toasterService.pop('error','Error', ((this.responseDataRemove)).message);
				}
			}
		);
	
	}
	
	
	
	
	
	
	/* This part of script is used for save withdraw amount */
	saveWithdrawAmount() {
	     console.log('this.withdrawamount');
		if(!this.withdrawamount) {
			this.toasterService.pop('error', 'Error', 'Amount is required.');			
		}else if(!this.userbanklist) {
			this.toasterService.pop('error', 'Error', 'Select one bank.');			
		}else if(!this.usersecurityanswer) {
			this.toasterService.pop('error', 'Error', 'Answer is required.');			
		}else{
			this._withdrawService.send_withdraw_amount(this._constantService.API_URL, this.logged_user_id, this.withdrawamount, this.userbanklist,this.usersecurityanswer,this.no_of_attempts_left,this.no_of_attempts)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						if(((this.responseData)).status == 200) {  
							this.toasterService.pop('success','Success', ((this.responseData)).message);
							this.router.navigate(['redirect', {redirect: 'withdraw'}]);
						}else if(((this.responseData)).status == 202){
							this.no_of_attempts_left --;
							this.no_of_attempts ++;
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}else  {
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}
					}
				);
			
			console.log(this.withdrawamount);
			
			/*this.router.navigate(['/login']);*/
		}
		
	} 
	
	
	/* This part of script is used for faq submission*/
	bankSubmit() {
	
		if(!this.bank_name){
			this.toasterService.pop('error', 'Error', 'Bank name is required.');			
		}if(!this.branch_code){
			this.toasterService.pop('error', 'Error', 'Bank code is required.');			
		}if(!this.account_no){
			this.toasterService.pop('error', 'Error', 'Account no is required.');			
		}else{
			this._withdrawService.add_new_bank(this._constantService.API_URL, this.logged_user_id, this.bank_id, this.bank_name, this.branch_code, this.account_no)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						if(((this.responseData)).status == 200) {                                                                                               
							this.toasterService.pop('success','Success', ((this.responseData)).message);
							console.log('ok');
							this.router.navigate(['redirect', {redirect: 'withdraw'}]);
						}else  {
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}
					}
				);
			 
			console.log(this.bank_name);
			console.log(this.branch_code);
			console.log(this.account_no);
			
			this.router.navigate(['/withdraw']);
		}
		
	}
	
	
}
