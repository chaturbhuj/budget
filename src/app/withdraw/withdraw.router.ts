import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { WithdrawComponent } from './withdraw.component';

const routes: Route[] = [
  {
    path: '',
    component: WithdrawComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
