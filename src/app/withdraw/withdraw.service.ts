import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class WithdrawService {

  private posts;
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

  constructor(public http: Http) {
	
	
  }
  
  /*this function is use to add bank*/

	add_new_bank(API_URL, user_id, bank_id, bank_name, branch_code, account_no){
	
		var params = {
			
			bank_name:bank_name,
			user_id:user_id,
			bank_id:bank_id,
			branch_code:branch_code,
			account_no:account_no,
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'Users/add_new_bank', params,{
				headers:headers 
		})
		.map((response:Response) => response.json());
	}
	
	/*this function is use to get user's bank details */
	get_user_bank_details(API_URL, user_id){	
		var params = {
			user_id:user_id,
		};		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		return this.http.post(API_URL+'Users/get_user_bank_details', params,{
				headers:headers 
			})
			.map((response:Response) => response.json());
	}
  
  
   /*this function is used to get the details of card user entered*/

	send_withdraw_amount(API_URL, user_id, withdrawamount, userbanklist, usersecurityanswer, no_of_attempts_left , no_of_attempts){
	
	var params = {
		user_id:user_id,
		withdrawamount:withdrawamount,
		userbanklist:userbanklist,
		usersecurityanswer:usersecurityanswer,
		no_of_attempts:no_of_attempts,
		no_of_attempts_left:no_of_attempts_left, 
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/send_withdraw_amount', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
  }
  
 
    /*this function is used to get the withdraw details of card user entered*/
  
	get_withdraw_details(API_URL, user_id, withdraw_id, withdraw_amount, withdraw_type){
	
	var params = {
		 withdraw_id: withdraw_id,
		 user_id:user_id,
		 withdraw_amount: withdraw_amount,
		 withdraw_type: withdraw_type,
	
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/get_withdraw_details', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
  }
  
  
  /*this function is use to remove user bank*/
	common_remove_bank(API_URL, user_id, bank_id, table_name, primary_key){	
		var params = {
			primary_id:bank_id,
			table_name:table_name,
			primary_key:primary_key,			
		};		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		return this.http.post(API_URL+'admin/common_remove_funcation', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
	}
	

}
