import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {WithdrawComponent } from './withdraw.component';
import { routing } from './withdraw.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
  WithdrawComponent
  ],
  bootstrap: [
   WithdrawComponent
  ]
})
export class  WithdrawModule {}
