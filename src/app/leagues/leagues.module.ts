import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LeaguesComponent } from './leagues.component';
import { routing } from './leagues.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    LeaguesComponent
  ],
  bootstrap: [
    LeaguesComponent
  ]
})
export class LeaguesModule {}
