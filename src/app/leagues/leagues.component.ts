import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from '@angular/router';

import { LeaguesService } from './leagues.service';
import { ConstantService } from '../constant/constant.service';


declare var $: any;

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html'
})
export class LeaguesComponent {
  /*Variable define */
	
	sport = '';
	
	public spportsData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private route: ActivatedRoute, private _leaguesService: LeaguesService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		 /*use to redirect url*/
		this.route.params.subscribe((params: any) => {
			this.sport = params['sport'];
			console.log(this.sport);
			if(!this.sport){
				this.router.navigate(['/home']);
			}
		});
		this._leaguesService.get_details_sports(this._constantService.API_URL,this.sport)
			.subscribe(
				data => this.spportsData = (data.leagues),
				error => alert(error),
				() => {
				console.log(this.spportsData);
				
				}
			);
	}
		
	ngOnInit() {} 
		
}
