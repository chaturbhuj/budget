import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class LeaguesService {

	constructor(public http: Http) {
	
	}

	get_details_sports(API_URL,sport){
		
		var params = {
				myurl:'v2/leagues?sportid='+sport,
		
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'configure/get_list_of_sports', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
  

}
