import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { ProfileSettingService } from './profile_setting.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from '@angular/router';

import { ConstantService } from '../constant/constant.service';
declare var $: any; 
     
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-profile_setting',
  templateUrl: './profile_setting.component.html'
})
export class ProfileSettingComponent implements OnInit {
	/*Variable define */
	
	user_email = '';
	user_name = '';
	firstname = '';
	lastname = '';
	rapidAddress = '';
	contactnumber = '';
	mrandms = '';
	dateofbirth = '';
	alternatecontactnumberr = '';
	newpass = '';
	oldpass = '';
	confnewpass = '';
	
	questionlist = '';
	security_answer = '';
	
	logged_user_id = 0;
	alternatecontactnumber='';
	private responseData:any = [];
	public responseanswerData:any = [];
	public responsequestionData:any = [];
	public responsefaqData:any = [];
	
	
	
	publicContent = true;
	privateContent = false;
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private route: ActivatedRoute,  private _constantService: ConstantService, private _profileSettingService: ProfileSettingService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		/* jquery statement of read the data of current user from database */
		var currentUser = JSON.parse(localStorage.getItem('budgetUser')); 
		  
		console.log(currentUser);
		if(currentUser){
			this.logged_user_id = currentUser.userID;
		} else{
			this.router.navigate(['/home']);
		}
		this._profileSettingService.profile_info_get(this._constantService.API_URL, this.logged_user_id)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						this.user_email=this.responseData.data[0].user_email;
						this.user_name=this.responseData.data[0].user_name;
						this.firstname=this.responseData.data[0].firstname;
						this.lastname=this.responseData.data[0].lastname;
						this.rapidAddress=this.responseData.data[0].rapidAddress;
						this.mrandms=this.responseData.data[0].mrandms;
						
						$("input[name='mrandms'][value='"+this.mrandms+"']").prop("checked",true);
						
						this.dateofbirth=this.responseData.data[0].dateofbirth;
						$('#dateofbirth').val(this.responseData.data[0].dateofbirth);
						
						this.alternatecontactnumberr=this.responseData.data[0].alternatecontactnumberr;
						this.contactnumber=this.responseData.data[0].contactnumber;
						
					}
				);
				
		this._profileSettingService.get_questions_list(this._constantService.API_URL, this.logged_user_id, this.questionlist)
				.subscribe(
					data => this.responsequestionData = (data.data),
					error => alert(error),
					() => {
						console.log(this.responsequestionData);
					}
				);
		this._profileSettingService.get_all_FAQ_list(this._constantService.API_URL, this.logged_user_id)
				.subscribe(
					data => this.responsefaqData = (data.data),
					error => alert(error),
					() => {
						console.log(this.responsefaqData);
					}
				)
				
	}
	
	
	
	ngOnInit() {}
	
	
	collapseOpenClose() {
		$('body').on('click','.myCollapsed',function(){
			$('.panel-collapse').removeClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').addClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').css({'height':'auto'});
		});
	}
	
	
	
	/* this is used to change password of user on profile setting 
	* made by:Hemlata Khatri
	*date:15-06-2017
	*time:16:27
	*/

	userChangePassword(){
		
		if (!this.oldpass) {
			this.toasterService.pop('error', 'Error', ' Old password is required.');			
		}else if (!this.newpass) {
			this.toasterService.pop('error', 'Error', ' New password is required.');			
		}else if (!this.confnewpass || this.confnewpass != this.newpass) {
			this.toasterService.pop('error', 'Error', 'Confirm Password is required and should be same as password.');
		}else{
			this._profileSettingService.change_password_user(this.newpass)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						
						if(((this.responseData)).status == 200) {
							this.toasterService.pop('success', 'Args Title1', 'Args Body1');
						}
					}
				);
			
			console.log(this.newpass);
			
			/*this.router.navigate(['/profile_setting']);*/
		
		}
		
	}
	
	
	
	update_profile_setting(){
		var mrandms = ($("input[type='radio'].mrandms:checked").val());
		console.log(mrandms);
		if (!this.firstname) {
				this.toasterService.pop('error', 'Error', ' First name is required.');			
			}else if (!this.lastname) {
				this.toasterService.pop('error', 'Error', 'Last name is required.');
			}else if (!this.rapidAddress) {
				this.toasterService.pop('error', 'Error', 'Address is required.');
			}else if (!this.contactnumber) {
				this.toasterService.pop('error', 'Error', 'Contact number is required.');
			}else if (!this.alternatecontactnumberr) {
				this.toasterService.pop('error', 'Error', ' Alternate Contact Number is required.');
			}else{
				this._profileSettingService.update_profile_info(this._constantService.API_URL, this.logged_user_id, mrandms, this.firstname, this.lastname, this.rapidAddress, $('#dateofbirth').val() , this.contactnumber, this.alternatecontactnumberr)
					.subscribe(
						data => this.responseData = (data),
						error => alert(error),
						() => {
							console.log(this.responseData);
							
							if(((this.responseData)).status == 200) {
								this.toasterService.pop('success', 'Success', ((this.responseData)).message);
								this.router.navigate(['redirect', {redirect: 'profile_setting'}]);
								
							}else{
								this.toasterService.pop('error', 'Error', ((this.responseData)).message);
							}
						}
					);
				
			
			
			/*this.router.navigate(['/profile_setting']);*/
		}
		
	}
	
	
	
	
	change_password_click_event(myClass) {		
		$('body').on('click','.change_password_change_event',function(){
			console.log('in');
			$(".heading_main").removeClass("profile_heading_active");
			$(this).addClass("profile_heading_active");
			$(".profile_setting_common").addClass("display_none");
			$("."+myClass).removeClass("display_none");
		}); 
	}	
	
	
	
	enableprofile() {		
		$('body').on('click','.edit-profile',function(){
			console.log('in');
			$(".profile-info").addClass("display_none");
			$(".edit-profile-info").removeClass("display_none");
		});
	}	
	
	
	
	doneprofile() {
		$('body').on('click','.done-profile',function(){
			$(".profile-info").removeClass("display_none");
			$(".edit-profile-info").addClass("display_none");
		});
	}	
	
	
	
	
	collapseopenclose() {
		$('body').on('click','.myCollapsed',function(){
			$('.panel-collapse').removeClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').addClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').css({'height':'auto'});
		});
	}
	
	
	
	/* This part of script is used for security amswer submission*/
	submit_security_answer() {
	
		if(!this.questionlist){
			this.toasterService.pop('error', 'Error', 'Select one question');			
		}else if(!this.security_answer){
			this.toasterService.pop('error', 'Error', 'Answer is required.');
		}else{
			this._profileSettingService.security_answer_submit(this._constantService.API_URL, this.logged_user_id, this.questionlist, this.security_answer)
				.subscribe(
					data => this.responseanswerData = (data),
					error => alert(error),
					() => {
						console.log(this.responseanswerData);
						
						if(((this.responseanswerData)).status == 200) {                                                                                               
							this.toasterService.pop('success','Success', ((this.responseanswerData)).message);
							console.log('ok');
							this.router.navigate(['redirect', {redirect: 'profile_setting'}]);
						}else  {
							this.toasterService.pop('error','Error', ((this.responseanswerData)).message);
						}
					}
				);
			
			console.log(this.security_answer);
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
}

