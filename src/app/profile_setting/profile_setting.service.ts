import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class  ProfileSettingService {

  private posts;
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

  constructor(public http: Http) {
	
	
  }
	/** =====================================================================
	 * This function is used to get the profile detail of loggedin user and userid is the most parameter in the function
	  * Created By: Hemalath
	  * Created Date : 13:06:2017
	  * Created time: 19:38
	 */
    profile_info_get(API_URL, user_id){
	
		var params = {
			user_id:user_id,
			
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'users/get_profile_info', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
    }

	
	/** =====================================================================
	 * This function is used to update the profile detail of loggedin user and userid is the most parameter in the function
	  * Created By: Hemlata
	  * Created Date : 13:06:2017
	  * Created time: 19:46
	 */
	
	update_profile_info(API_URL, user_id, mrandms, firstname, lastname, rapidAddress, dateofbirth, contactnumber, alternatecontactnumberr){
	
	
		var params = {
			user_id:user_id,
			mrandms:mrandms,
			firstname:firstname,
			lastname:lastname,
			rapidAddress:rapidAddress,
			dateofbirth:dateofbirth,
			contactnumber:contactnumber,
			alternatecontactnumberr:alternatecontactnumberr,
			
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'users/update_profile', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
    }
	
	/** =====================================================================
	 * This function is used to change the password of loggedin user
	  * Created By: Hemlata
	  * Created Date : 15:06:2017
	  * Created time: 16:35
	 */
	
	change_password_user(newpass){
  
	var params = {
		newpass:newpass,
	};

	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(this.url1, params,{
			headers:headers
		})
		.map((response:Response) => response.json());
  }
	
/*this function is use to add security answer*/

	security_answer_submit(API_URL, user_id, questionlist, security_answer){
	
		var params = {
			user_id:user_id,
			questionlist:questionlist,
			security_answer:security_answer,
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		return this.http.post(API_URL+'users/security_answer_submit', params,{
				headers:headers 
		})
		.map((response:Response) => response.json());
	}
	
	/** =====================================================================
	 * This function is used to get the questions list userid is the most parameter in the function
	  * Created By: Hemalata
	  * Created Date : 19:06:2017
	  * Created time: 17:53
	 */
    get_questions_list(API_URL, user_id,questionlist){
	
		var params = {
			user_id:user_id,
			questionlist:questionlist,
			
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'users/get_questions_list', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
    }

	/** =====================================================================
	 * This function is used to get the questions list userid is the most parameter in the function
	  * Created By: Hemalata
	  * Created Date : 19:06:2017
	  * Created time: 17:53
	 */
    get_all_FAQ_list(API_URL, user_id){
	
		var params = {
			user_id:user_id,
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		
		return this.http.post(API_URL+'users/get_all_FAQ_list', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
    }
	
}	