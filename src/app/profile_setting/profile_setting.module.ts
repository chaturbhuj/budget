import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ProfileSettingComponent } from './profile_setting.component';
import { routing } from './profile_setting.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
  ProfileSettingComponent
  ],
  bootstrap: [
   ProfileSettingComponent
  ]
})
export class ProfileSettingModule {}

