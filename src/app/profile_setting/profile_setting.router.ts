import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ProfileSettingComponent } from './profile_setting.component';

const routes: Route[] = [
  {
    path: '',
    component:  ProfileSettingComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

