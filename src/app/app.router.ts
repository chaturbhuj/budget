import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Route[] = [ 
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { loadChildren: 'app/register/register.module#RegisterModule', path: 'register' },
  { loadChildren: 'app/public_content/aboutus.module#AboutusModule', path: 'aboutus' },
  { loadChildren: 'app/dashboard/dashboard.module#DashboardModule', path: 'dashboard' },
  { loadChildren: 'app/profile/profile.module#ProfileModule', path: 'profile' },
  { loadChildren: 'app/home/home.module#HomeModule', path: 'home' },
  { loadChildren: 'app/forgot_password/forgot_password.module#ForgotPasswordModule', path: 'forgot_password' },
  { loadChildren: 'app/profile_setting/profile_setting.module#ProfileSettingModule', path: 'profile_setting' },
  { loadChildren: 'app/profile_security_setting/profile_security_setting.module#ProfileSecuritySettingModule', path: 'profile_security_setting' },
  { loadChildren: 'app/public_content/regulator.module#RegulatorModule', path: 'regulator' },
  { loadChildren: 'app/public_content/terms_and_conditions.module#TermsAndConditionsModule', path: 'terms_and_conditions' },
  { loadChildren: 'app/public_content/privacy_policy.module#PrivacyPolicyModule', path: 'privacy_policy' },
  { loadChildren: 'app/public_content/credit_policy.module#CreditPolicyModule', path: 'credit_policy' },
  { loadChildren: 'app/public_content/support.module#SupportModule', path: 'support' },
  { loadChildren: 'app/public_content/responsible_gambling.module#ResponsibleGamblingModule', path: 'responsible_gambling' },
  { loadChildren: 'app/my_wallet/my_wallet.module#MyWalletModule', path: 'my_wallet' },
  { loadChildren: 'app/leagues/leagues.module#LeaguesModule', path: 'leagues' },
  { loadChildren: 'app/fixtures/fixtures.module#FixturesModule', path: 'fixtures' },
  { loadChildren: 'app/withdraw/withdraw.module#WithdrawModule', path: 'withdraw' },

  { loadChildren: 'app/redirect/redirect.module#RedirectModule', path: 'redirect' },
  { loadChildren: 'app/weather/weather.module#WeatherModule', path: 'weather' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(
  routes,
  {
    useHash: true
  }  
);
