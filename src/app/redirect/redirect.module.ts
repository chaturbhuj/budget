import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RedirectComponent } from './redirect.component';
import { routing } from './redirect.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    RedirectComponent
  ],
  bootstrap: [
    RedirectComponent
  ]
})
export class RedirectModule {}
