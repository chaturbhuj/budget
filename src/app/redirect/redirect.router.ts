import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { RedirectComponent } from './redirect.component';

const routes: Route[] = [
  {
    path: '',
    component: RedirectComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
