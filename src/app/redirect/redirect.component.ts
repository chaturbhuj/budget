import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute} from '@angular/router';


declare var $: any;

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html'
})
export class RedirectComponent {
	constructor(private router: Router, private route: ActivatedRoute) {		
		
		this.route.params.subscribe((params: any) => {
			let redirect_url = params['redirect'];
			console.log(redirect_url);
			this.router.navigate([redirect_url]);
		});
		
	}
}
