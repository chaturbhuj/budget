import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class RegisterService {

	constructor(public http: Http) {
	
	}

	register_user(API_URL, user_email, firstname, lastname, passwd){
		
		var params = {
			user_email:user_email,
			firstname:firstname,
			lastname:lastname,
			passwd:passwd
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'users/register', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
  

}
