import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import { RegisterService } from './register.service';
import { ConstantService } from '../constant/constant.service';


import 'rxjs/add/operator/map';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
	/*Variable define */
	user_email = '';
	firstname = '';
	lastname = '';
	passwd = '';
	cfm_passwd = '';
	private responseData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _registerService: RegisterService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
				
	}
		
	ngOnInit() {}
		
	/* This part of script is used for user registration*/
	userRegistration() {	
		
		var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
		if (!this.user_email) {
			this.toasterService.pop('error', 'Error', 'Email is required.');
		}else if (!EMAIL_REGEXP.test(this.user_email)) {
			this.toasterService.pop('error', 'Error', 'Email is not valid.');
		}else if (!this.firstname) {
			this.toasterService.pop('error', 'Error', 'First name is required.');		
		}else if (!this.lastname) {
			this.toasterService.pop('error', 'Error', 'Last name is required.');		
		}else if (!this.passwd) {
			this.toasterService.pop('error', 'Error', 'Password is required.');		
		}else if (!this.cfm_passwd || this.cfm_passwd != this.passwd) {
			this.toasterService.pop('error', 'Error', 'Confirm Password is required and should be same as password.');		
		}else{
			this._registerService.register_user(this._constantService.API_URL, this.user_email, this.firstname, this.lastname, this.passwd)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						if(((this.responseData)).status == 200) {
							this.toasterService.pop('success', 'Success', ((this.responseData)).message);
							this.router.navigate(['/home']);
						}else{
							this.toasterService.pop('error', 'Error', ((this.responseData)).message);
						}
					}
				);
			
			/*this.router.navigate(['/login']);*/
		}
		
	}
	
}
