import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ToasterModule, ToasterService} from 'angular2-toaster';

import { AppComponent } from './app.component';
import { routing } from './app.router';
import { effects, store, instrumentation } from './store';
import { SharedModule } from './shared/shared.module';
import { WeatherService } from './weather/weather.service';
import { TopNavigationService } from './shared/top-navigation/top-navigation.service';
import { RegisterService } from './register/register.service';
import { AboutusService } from './public_content/aboutus.service';
import { ConstantService } from './constant/constant.service';
import { ForgotPasswordService } from './forgot_password/forgot_password.service';
import { ProfileSettingService } from './profile_setting/profile_setting.service';
import { ProfileSecuritySettingService } from './profile_security_setting/profile_security_setting.service';
import { HomeService } from './home/home.service';
import { LeaguesService } from './leagues/leagues.service';
import { FixturesService } from './fixtures/fixtures.service';
import { MyWalletService } from './my_wallet/my_wallet.service';
import { WithdrawService } from './withdraw/withdraw.service';



	
@NgModule({
  declarations: [
    AppComponent 
  ],
  imports: [
    BrowserModule,
    SharedModule,
    FormsModule,  
    HttpModule,
    BrowserAnimationsModule,
    ToasterModule,
    store,
    effects,
    routing,
    instrumentation
  ], 
  providers: [
    WeatherService , ToasterService , TopNavigationService , RegisterService, 
	ConstantService, ForgotPasswordService, ProfileSettingService, ProfileSecuritySettingService,
	AboutusService, MyWalletService, HomeService, LeaguesService, FixturesService, WithdrawService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
