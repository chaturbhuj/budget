import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import { AboutusService } from './aboutus.service';
import { ConstantService } from '../constant/constant.service';


import 'rxjs/add/operator/map';


@Component({
  selector: 'app-credit_policy',
  templateUrl: './credit_policy.component.html'
})
export class CreditPolicyComponent implements OnInit {
	/*Variable define */
	content_id = 1;
	content='';
	
	private responseData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _aboutusService: AboutusService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		this._aboutusService.aboutus_user(this._constantService.API_URL, this.content_id)
			.subscribe(
				data => this.responseData = (data),
				error => alert(error),
				() => {
				this.content=(this.responseData).data[0].content;
				if(((this.responseData)).status == 200) {                                                                                               
					this.toasterService.pop('success','Success', ((this.responseData)).message);
				}else {
					this.toasterService.pop('error','Error', ((this.responseData)).message);
				}
				}
			);
	}
		
	ngOnInit() {}
		
	
}
