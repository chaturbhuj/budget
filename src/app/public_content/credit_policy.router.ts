import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { CreditPolicyComponent } from './credit_policy.component';

const routes: Route[] = [
  {
    path: '',
    component: CreditPolicyComponent
  }
]; 

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
