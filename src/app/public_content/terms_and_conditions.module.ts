import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TermsAndConditionsComponent } from './terms_and_conditions.component';
import { routing } from './terms_and_conditions.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   TermsAndConditionsComponent
  ],
  bootstrap: [  
    TermsAndConditionsComponent
  ] 
})
export class TermsAndConditionsModule {}
