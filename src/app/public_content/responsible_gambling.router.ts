import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { ResponsibleGamblingComponent } from './responsible_gambling.component';

const routes: Route[] = [
  {
    path: '',
    component: ResponsibleGamblingComponent
  }
]; 

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
 