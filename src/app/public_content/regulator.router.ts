import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { RegulatorComponent } from './regulator.component';

const routes: Route[] = [
  {
    path: '',
    component: RegulatorComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
