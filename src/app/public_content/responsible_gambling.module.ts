import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ResponsibleGamblingComponent } from './responsible_gambling.component';
import { routing } from './responsible_gambling.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   ResponsibleGamblingComponent
  ],
  bootstrap: [  
    ResponsibleGamblingComponent
  ] 
})
export class ResponsibleGamblingModule {}
  