import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreditPolicyComponent } from './credit_policy.component';
import { routing } from './credit_policy.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   CreditPolicyComponent
  ],
  bootstrap: [  
    CreditPolicyComponent
  ] 
})
export class  CreditPolicyModule {}
  