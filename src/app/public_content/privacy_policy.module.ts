import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PrivacyPolicyComponent } from './privacy_policy.component';
import { routing } from './privacy_policy.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   PrivacyPolicyComponent
  ],
  bootstrap: [  
    PrivacyPolicyComponent
  ] 
})
export class PrivacyPolicyModule {}
  