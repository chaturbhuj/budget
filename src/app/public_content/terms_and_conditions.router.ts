import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { TermsAndConditionsComponent } from './terms_and_conditions.component';

const routes: Route[] = [
  {
    path: '',
    component: TermsAndConditionsComponent
  }
]; 

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
