import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RegulatorComponent } from './regulator.component';
import { routing } from './regulator.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   RegulatorComponent
  ],
  bootstrap: [
    RegulatorComponent
  ] 
})
export class RegulatorModule {}
