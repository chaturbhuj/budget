import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class AboutusService {

	constructor(public http: Http) {
	
	}

	aboutus_user(API_URL, content_id){
		
		var params = {
				content_id:content_id,
		
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'configure/get_content', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
  

}
