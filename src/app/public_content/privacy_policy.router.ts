import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PrivacyPolicyComponent } from './privacy_policy.component';

const routes: Route[] = [
  {
    path: '',
    component: PrivacyPolicyComponent
  }
]; 

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
