import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { SupportComponent } from './support.component';

const routes: Route[] = [
  {
    path: '',
    component: SupportComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
