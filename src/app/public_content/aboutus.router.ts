import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { AboutusComponent } from './aboutus.component';

const routes: Route[] = [
  {
    path: '',
    component: AboutusComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
