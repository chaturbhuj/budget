import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AboutusComponent } from './aboutus.component';
import { routing } from './aboutus.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
    AboutusComponent
  ],
  bootstrap: [
    AboutusComponent
  ]
})
export class AboutusModule {}
