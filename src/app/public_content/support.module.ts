import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SupportComponent } from './support.component';
import { routing } from './support.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ], 
  declarations: [
   SupportComponent
  ],
  bootstrap: [
    SupportComponent
  ] 
})
export class SupportModule {}
