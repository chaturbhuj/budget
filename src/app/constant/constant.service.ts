import { Injectable } from '@angular/core';


@Injectable()
export class ConstantService {
	
	/* Local server */
	public APP_NAME = 'Budgetbet';
	public APP_BASE_URL = 'http://localhost/budgetbet/';
	public X_API_KEY = '123456';
	public PRODUCT_NAME = 'Budgetbet';
	public PRODUCT_NAME_MINI = 'Budgetbet';
	public PRODUCT_LOGO = 'assets/images/logo.png';
	public PRODUCT_FAVICON = 'assets/images/favicon.png';

	public API_URL = 'http://localhost/budgetbetapi/index.php/api/';

	public API_BASE_URL = 'http://localhost/budgetbetapi/';


	
	
	/* server system
	public APP_NAME = 'Budgetbet';
	public APP_BASE_URL = 'http://budgetbetapi.lennoxsoft.in/';
	public X_API_KEY = '123456';
	public PRODUCT_NAME = 'Budgetbet';
	public PRODUCT_NAME_MINI = 'Budgetbet';
	public PRODUCT_LOGO = 'assets/images/logo.png';
	public PRODUCT_FAVICON = 'assets/images/favicon.png';

	public API_URL = 'https://budap.herokuapp.com/index.php/api/';

	public API_BASE_URL = 'https://budap.herokuapp.com/'; */
	

	constructor() { } 
}
