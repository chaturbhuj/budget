import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {

	constructor(public http: Http) {
	
	}

	get_sports(API_URL,myurl){
		
		var params = {
				myurl:'v2/sports',
		
		};
		
		var headers = new Headers();
		headers.append('Content-Type','application/json') 
		console.log(API_URL);
		return this.http.post(API_URL+'configure/get_list_of_sports', params,{
				headers:headers
			})
			.map((response:Response) => response.json());
	}
  

}
