import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import { HomeService } from './home.service';
import { ConstantService } from '../constant/constant.service';


declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  /*Variable define */
	myurl:'';
	
	
	public spportsData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _homeService: HomeService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		this._homeService.get_sports(this._constantService.API_URL,this.myurl)
			.subscribe(
				data => this.spportsData = (data.sports),
				error => alert(error),
				() => {
				console.log(this.spportsData);
				
				}
			);
	}
		
	ngOnInit() {}
		
}
