import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';
import { ProfileSecuritySettingService } from './profile_security_setting.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import { ConstantService } from '../constant/constant.service';
declare var $: any;
     
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-profile_security_setting',
  templateUrl: './profile_security_setting.component.html'
})
export class  ProfileSecuritySettingComponent implements OnInit {
	/*Variable define */
	
	logged_user_id = 0;
	
	private responseData:any = [];
	oldpassword='';
	newpassword='';
	confirmed_password='';
	publicContent = true;
	privateContent = false;
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService,  private _profileSecuritySettingService: ProfileSecuritySettingService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		/* This script is used to read user session data from his local storage */
		var currentUser = JSON.parse(localStorage.getItem('budgetUser')); 
		console.log(currentUser);
		if(currentUser){
			this.logged_user_id = currentUser.userID;
		} else{
			this.router.navigate(['/home']);
		}
	}
	
	
	ngOnInit() {}
	
	collapseOC() {
		$('body').on('click','.myCollapsed',function(){
			$('.panel-collapse').removeClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').addClass('in');
			$(this).closest('div.panel-default').find('.panel-collapse').css({'height':'auto'});
		});
	}
	
	
	
	
	updatePassword(){
	
		if (!this.oldpassword){
			this.toasterService.pop('error', 'Error', 'Old Password is required.');			
		}else if (!this.newpassword) {
			this.toasterService.pop('error', 'Error', 'Password is required.');		
		}else if (!this.confirmed_password || this.confirmed_password != this.newpassword) {
			this.toasterService.pop('error', 'Error', 'Confirm Password is required and should be same as password.');		
		}
		else{ 
			this._profileSecuritySettingService.profile_security_setting_user(this._constantService.API_URL, this.logged_user_id, this.oldpassword, this.newpassword)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						if(((this.responseData)).status == 200) {                                                                                               
							this.toasterService.pop('success','Success', ((this.responseData)).message);
						}else  {
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}
					}
				);
				this.router.navigate(['/profile_security_setting']); 
			
			}
		} 
	}
