import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ProfileSecuritySettingComponent } from './profile_security_setting.component';
import { routing } from './profile_security_setting.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
  ProfileSecuritySettingComponent
  ],
  bootstrap: [
   ProfileSecuritySettingComponent
  ]
})
export class  ProfileSecuritySettingModule {}

