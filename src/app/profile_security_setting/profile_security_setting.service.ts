import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class  ProfileSecuritySettingService {

  private posts;
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

  constructor(public http: Http) {
	
	
  }
  

profile_security_setting_user(API_URL, user_id, oldpassword, newpassword){
	
	var params = {
		oldpassword:oldpassword,
		newpassword:newpassword,
		user_id:user_id,
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/send_change_password', params,{
			headers:headers
		})
		.map((response:Response) => response.json());
  }

}
