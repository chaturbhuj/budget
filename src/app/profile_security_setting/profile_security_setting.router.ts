import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ProfileSecuritySettingComponent } from './profile_security_setting.component';

const routes: Route[] = [
  {
    path: '',
    component:  ProfileSecuritySettingComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

