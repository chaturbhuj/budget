import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MyWalletComponent } from './my_wallet.component';

const routes: Route[] = [
  {
    path: '',
    component: MyWalletComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
