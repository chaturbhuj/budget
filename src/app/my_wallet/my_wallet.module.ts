import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MyWalletComponent } from './my_wallet.component';
import { routing } from './my_wallet.router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    routing
  ],
  declarations: [
  MyWalletComponent
  ],
  bootstrap: [
   MyWalletComponent
  ]
})
export class  MyWalletModule {}
