import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { HttpModule, Response } from '@angular/http';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { MyWalletService } from './my_wallet.service';
import { ConstantService } from '../constant/constant.service';


import 'rxjs/add/operator/map';

	declare var $: any;
	
@Component({
  selector: 'app-my_wallet',
  templateUrl: './my_wallet.component.html'
})
export class MyWalletComponent implements OnInit {

	/*Variable define */

	logged_user_id = 0;
	transaction_id = 0;
	card_type = '';
	amount = '';
	cardnumber = '';
	expmonth = '';
	expyear = '';
	public responseData:any = [];
	public responseCardData:any = [];
	
	private toasterService: ToasterService;
	
	constructor(private router: Router, private _constantService: ConstantService, private _myWalletService: MyWalletService, toasterService: ToasterService) {		
		this.toasterService = toasterService;
		
		/* This script is used to read user session data from his local storage */
		var currentUser = JSON.parse(localStorage.getItem('budgetUser')); 
		console.log(currentUser);
		if(currentUser){
			this.logged_user_id = currentUser.userID;		
		} else{
			this.router.navigate(['/home']);
		}
		
		this._myWalletService.get_card_details(this._constantService.API_URL, this.logged_user_id, this.transaction_id, this.amount, this.cardnumber, this.card_type)
			.subscribe(
				data => this.responseCardData = (data.data),
				error => alert(error),
				() => {
					console.log(this.responseCardData);
				}
			);
	
	}
	
	
	ngOnInit() {}
	
	/* This part of script is used for save card details*/
	saveCardDetails() {
	     
		 if(!this.amount) {
			this.toasterService.pop('error', 'Error', 'Amount is required.');			
		}else if(!this.cardnumber) {
			this.toasterService.pop('error', 'Error', 'Card number is required.');			
		}else if (!this.expmonth) {
			this.toasterService.pop('error', 'Error', 'Expiry month is not valid.');
		}else if (!this.expyear) {
			this.toasterService.pop('error', 'Error', 'Expiry year is not valid.');
		}else{
			this._myWalletService.send_card_details(this._constantService.API_URL, this.logged_user_id, this.amount, this.cardnumber, this.expmonth, this.expyear)
				.subscribe(
					data => this.responseData = (data),
					error => alert(error),
					() => {
						console.log(this.responseData);
						
						if(((this.responseData)).status == 200) {                                                                                               
							$('#depositnow').modal('show');
							this.amount='';
							this.cardnumber='';
							this.expmonth='';
							this.expyear='';
							
						}else  {
							this.toasterService.pop('error','Error', ((this.responseData)).message);
						}
					}
				);
			
			console.log(this.cardnumber);
			
			/*this.router.navigate(['/login']);*/
		}
		
	}
	
	
	
}
