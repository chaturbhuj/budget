import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class MyWalletService {

  private posts;
  private url1 = 'http://psapi.lennoxsoft.in/index.php/api/configure_access/get_subOperator_list'

  constructor(public http: Http) {
	
	
  }
  
  
  /*this function is used to send the details of card user entered*/

 send_card_details(API_URL, user_id, amount, cardnumber, expmonth, expyear){
	
	var params = {
		user_id:user_id,
		amount:amount,
		cardnumber:cardnumber,
		expmonth:expmonth,
		expyear:expyear,
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/send_card_details', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
  }
  
  
  
    /*this function is used to get the details of card user entered*/
  
   get_card_details(API_URL, transaction_id, user_id, amount, card_number, card_type){
	
	var params = {
		transaction_id:transaction_id,
		user_id:user_id,
		amount:amount,
		card_number:card_number,
		card_type:card_type,
	};
	
	var headers = new Headers();
	headers.append('Content-Type','application/json') 
	
	return this.http.post(API_URL+'users/get_card_details', params,{
			headers:headers 
		})
		.map((response:Response) => response.json());
  }

}
