import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LeaguesComponent } from './leagues.component';

const routes: Route[] = [
  {
    path: '',
    component: LeaguesComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
